﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BobbleSort
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] arr = { 800, 11, 50, 771, 649, 770, 240, 9 };
            int len = arr.Length;
            for (int i = 0; i < len; i++)
            {
                int limit = len - i - 1;
                for (int j = 0; j < limit; j++)
                {
                    if(arr[j] > arr[j+1])
                    {
                        int temp = arr[j+1];
                        arr[j+1] = arr[j];
                        arr[j] = temp;
                    }
                   /* 
                   foreach (int n in arr)
                    {
                        Console.Write(" " + n);
                    }
                    Console.Write("\n");
                    */
                }
                
            }
            foreach (int n in arr)
            {
                Console.Write(" " + n);
            }
            Console.ReadKey();
        }
    }
}
